package com.example.androidstudio.prelimorgaafternoon_3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {

    private TextView mReversedText;
    private TextView mCountText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        mReversedText = (TextView) findViewById(R.id.reversedText);
        mCountText = (TextView) findViewById(R.id.countText);

        Intent intent = getIntent();
        String input = intent.getStringExtra("input");

        finishIt(input);
    }

    private void finishIt(String input) {
        String reversedText = new StringBuilder(input).reverse().toString();

        mReversedText.setText(reversedText);

        int[] letterCount = countRedundancy(input);

        mCountText.setText("");
        for (int i = 0; i < letterCount.length; i++) {
            int count = letterCount[i];
            char chr = (char) (i + 97);

            if (count > 1)
                mCountText.setText(mCountText.getText().toString() + chr + " = " + count + "\n");
        }
    }

    private int[] countRedundancy(String str) {

        int[] letterCount = new int[26];

        for (int i = 0; i < str.length(); i++) {
            char chr = str.charAt(i);

            if (chr >= 97 && chr <= 123)
                letterCount[chr - 97]++;
        }

        return letterCount;
    }
}
